#-*- coding:utf-8 -*-

"""
Simple static page generator which can compile any type of templates.

The module is broken into three components:

    Environment - A way to find and load templates to be rendered
    RenderFunc  - A function that renders a given template with a context
    Renderer    - The controller who uses the environment to load templates and
                  who uses the RenderFunc to render the loaded template.

"""

from __future__ import absolute_import

__version__ = "0.0.1"
__author__ = "Jordan Sherer <jordan@widefido.com>"

import argparse
import codecs
import copy
import datetime
import inspect
import json
import logging
import os
import re

logging.root.setLevel(logging.NOTSET)

log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


class RendererError(Exception):
    pass


class TemplateEnvironment(object):
    """
    An TemplateEnvironment is a way to find and load templates.

    """
    def __init__(self, *args, **kwargs):
        pass

    def list_templates(self, template_filter=None):
        """ returns a list of templates, filtered by `template_filter` """
        return []

    def get_template(self, template_name):
        """ return the contents of a single template with `template_name` """
        return ""
    

class Renderer(object):
    """
    The renderer object.

    :param template_dir <string> the directory containing the templates. Defaults to ``'templates'``
    :param contexts     <list>   list of regex-function pairs. the function should return a context for that template. the regex, if matched against a filename, will cause the context to be used.
    :param rules        <list>   used to override template compilation. The value of rules should be a list of `regex, function` pairs where `function` takes a jinja2 Environment, the filename, and the context and renders the template, and `regex` is a regex that if matched against a filename will cause `function` to be used instead of the default.

    """

    def __init__(self,
            environment, 
            build_dir="build",
            contexts=None,
            rules=None):

        if not environment or not hasattr(environment, "list_templates") or \
                not hasattr(environment, "get_template"):
            raise RendererError("a valid `environment` is required")

        self.environment = environment
        self.build_dir = build_dir

        self.contexts = contexts or []
        self.rules = rules or []


    def list_templates(self):
        """
        Get a list of templates from the current environment.

        """
        return self.environment.list_templates()


    def get_template(self, template_name):
        """
        Get a template object from the environment.

        :param template_name: the name of the template

        """
        return self.environment.get_template(template_name)


    def get_context(self, template_name):
        """
        Get the context for a template.

        By default, this function will return an empty context.

        If a matching context_generator can be found, it will be executed 
        with the renderer, the template_name and template contents as 
        its arguments.        

        The context generator should return a dictionary
        representing the context.

        :param template_name: the name of the template

        """
        if self.contexts:
            for regex, context_generator in self.contexts:
                if re.match(regex, template_name):
                    try:
                        return context_generator(self, template_name, self.get_template(template_name))
                    except TypeError:
                        return context_generator(self, template_name)
        return {}


    def render_templates(self):
        """
        Render each of the templates.
        
        """
        templates = self.list_templates()
        if templates:
            log.info("Rendering templates:")
            for template_name in templates:
                self.render_template(template_name)
        else:
            log.warning("No templates to render")


    def render_template(self, template_name):
        """
        Render a template by checking for a matching render rule.
        
        If no matching render rule is found, the template will not be rendered.

        :param template_name: the name of the template

        """
        log.info("- rendering {0}".format(template_name))

        template = self.get_template(template_name)
        context = self.get_context(template_name)

        for regex, render_func in self.rules:
            if re.match(regex, template_name):
                render_func(self, template_name, template, context)
                return

        log.warning("  - template {0} could not be rendered "
            "(no matching render rule)".format(template_name))


class JinjaTemplateEnvironment(TemplateEnvironment):
    """ Jinja Template Environment provides a Jinja2 FileSystemLoader """
    def __init__(self, base_dir, encoding="utf-8", extensions=None):
        self.base_dir = base_dir
        self.encoding = encoding
        if not extensions:
            extensions = []
        self.extensions = extensions

        import jinja2
        self.loader = jinja2.FileSystemLoader(searchpath=self.base_dir,
            encoding=self.encoding)
        self.environment = jinja2.Environment(loader=self.loader, extensions=self.extensions)
    
    def filter_func(self, filename):
        """
        Check if the file should be rendered.
    
        Hidden files will not be rendered.
    
        Files prefixed with an underscore are assumed to be partials and will
        not be rendered.
    
        :param filename: the name of the file to check
    
        """
        _, tail = os.path.split(filename)
        return not (tail.startswith('_') or tail.startswith("."))

    def list_templates(self):
        """
        return a list of template names relative to the base directory and
        filtered by the provided filter_func.
        
        """
        return self.environment.list_templates(filter_func=self.filter_func)

    def get_template(self, *args, **kwargs):
        """
        return a template object that will be passed to the render function.

        """
        return self.environment.get_template(*args, **kwargs)

def compute_jinja_template_context(renderer, template_name, template):
    return {
        "last_build":datetime.datetime.now(),
    	"template_name":template_name if template_name else "",
        "templates":list(renderer.list_templates()) if renderer else []
    }

def render_jinja_template(renderer, template_name, template, context):
    """ Render a jinja template """
    build_dir = renderer.build_dir
    output_path = os.path.join(build_dir, template.name)

    head, tail = os.path.split(output_path)
    if head and not os.path.exists(head):
        os.makedirs(head)

    output = template.render(**context)
    with codecs.open(output_path, "w", "utf8") as f:
        f.write(output)

def walk_dir(start_dir,
        directory_match=None,
        directory_filter=None,
        directory_yield=False,
        directory_callback=None,
        file_match=None,
        file_filter=None,
        file_yield=False,
        file_callback=None, 
        match_function=None,
        max_depth=-1):
    """
    Walk a directory in level order and either yield or callback as each file
    or directory is seen. Optionally specify filters and match functions and 
    maximum depth for walking.

    >>> walk_dir(".", directory_yield=True, max_depth=1)
    <generator object walk_dir at 0x103a63fa0>

    :param start_dir          <string>   the base directory to walk
    :param directory_match    <string>   string to match directories against
        (using the match_function provided or fnmatch)
    :param directory_filter   <callable> a callable that will be called with
        the directory path as the first parameter, which should return true if
        the provided directory matches
    :param directory_yield    <boolean>  yield from this function as we match
        against directories
    :param directory_callback <callable> a callbale that will be called when
        we match a directory
    :param file_match         <string>   same as directory_match for files
    :param file_filter        <callable> same as directory_filter for files
    :param file_yield         <boolean>  same as directory_yield for files 
    :param file_callback      <callable> same as directory_callback for files 
    :param match_function     <callable> a callable that will be called with
        the file/directory name as the first parameter and the
        file/directory_match as the second parameter for each directory and
        file, which should return true if the provided match string matches
    :param max_depth          <integer>  the maximum depth to recurse 

    """
    import fnmatch
    import heapq
    import os

    stack = [start_dir]
    depth = 0
    match = match_function if callable(match_function) else fnmatch.fnmatch
    
    while stack:
        directory = heapq.heappop(stack)
        
        subdirs = 0
        
        for base in os.listdir(directory):
            name = os.path.join(directory, base)
            if os.path.isdir(name):
                heapq.heappush(stack, name)
                subdirs += 1
                if (not directory_match or match(name, directory_match)) and \
                   (not directory_filter or directory_filter(name)):
                    if callable(directory_callback):
                        directory_callback(name, depth)
                    if directory_yield:
                        yield (name, depth)
            else:
                if (not file_match or match(name, file_match)) and \
                   (not file_filter or file_filter(name)):
                    if callable(file_callback):
                        file_callback(name, depth)
                    if file_yield:
                        yield (name, depth)
        if subdirs:
            depth += 1
        if max_depth > -1 and depth > max_depth:
            break


def run(environment=None, build_dir="build", templates_dir="templates", global_context_file=None, contexts=None, rules=None):
    """ By default, we use JinjaTemplateEnvironment and a jinja2 render function """
    if not environment:
        environment = JinjaTemplateEnvironment(templates_dir, extensions=["jinja2.ext.do"])

    if not contexts:
    	contexts = [
    	    ("(.+)\.html$", compute_jinja_template_context)
        ]

    if global_context_file and os.path.exists(global_context_file):
        try:
            with open(global_context_file, "r") as f:
                log.info("Loading context file: %s", global_context_file)
                context = json.loads(f.read())
                def wrapped(context_func):
                    def _context(renderer, template_name, template):
                        d = {"global":copy.deepcopy(context)}
                        d.update(context_func(renderer, template_name, template))
                        return d
                    return _context
                for i, c in enumerate(contexts):
                	contexts[i] = (c[0], wrapped(c[1])) 
        except:
            pass

    if not rules:
        rules = [
            ("(.+)\.html$", render_jinja_template)
        ]


    log.info("Starting {0} => {1}".format(templates_dir, build_dir))

    renderer = Renderer(environment,
        build_dir=build_dir,
        contexts=contexts,
        rules=rules)

    renderer.render_templates()


def cli():
    """ Handle the command line arguments of the app """
    parser = argparse.ArgumentParser(description="render templates into static files")
    parser.add_argument("-v", dest="verbose", action="store_true", help="be more verbose")
    parser.add_argument("-q", dest="quiet", action="store_true", help="be quiet (less verbose)")
    parser.add_argument("-g", "--global-context-file", default="", help="path to a context json file that will be supplied to the templates under the 'global' var (default: global.json)")
    parser.add_argument("-i", "--input-dir", default="templates", metavar="DIR", help="path to templates directory (default: ./templates)")
    parser.add_argument("-o", "--output-dir", default="build", metavar="DIR", help="path to build directory (default: ./build)")

    args = parser.parse_args()

    if args.quiet:
        log.setLevel(logging.WARNING)
    elif args.verbose:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)

    try:
        run(build_dir=args.output_dir, templates_dir=args.input_dir, global_context_file=args.global_context_file)
    except KeyboardInterrupt:
        print()


if __name__ == "__main__":
    cli()
