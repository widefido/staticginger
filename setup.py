from distutils.core import setup

from staticginger import __version__

setup(
    name="staticginger",
    version=__version__,
    description="simple and generic static HTML compiler",
    author="Jordan Sherer",
    author_email="jordan@widefido.com",
    url="http://bitbucket.org/widefido/staticginger",
    keywords=["jinja", "static", "website"],
    py_modules=["staticginger"],
    scripts=["scripts/staticginger"],
    classifiers=[
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3.2",
        "Programming Language :: Python :: 3.3",
        "Development Status :: 4 - Beta",
    ],
)
