# Static Ginger

A simple utility to generate html files from a directory of templates.

## Background

Inspired by staticjinja, Static Ginger abstracts the template environment and
templating compiler to be generic so it the system can combined with any 
templating system. All that is required are two functions:

    def find_templates(base_directory):
        """ return a list of templates to compile in the template directory """
        return []

    def render_template(context, path):
        """
        return a compiled template string for the provided template with 
        the provided context applied.
        
        """
        return ""

## Supported Templating Engines

- jinja2
- <your engine here>...
